import { Component, OnInit } from '@angular/core';
import userData from '../../datas/userData.json';
import { User } from '../../models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  users = userData;

  displayedColumns: string[] = [
    'id',
    'name',
    'occupation',
    'detail',
    'update',
    'delete',
  ];

  ngOnInit() {}
  constructor(private router: Router) {}

  viewDetails(user: User): void {
    this.router.navigate(['/user', user.id]);
  }

  updateUser(user: User): void {
    this.router.navigate(['/update', user.id]);
  }

  deleteUser(user: User): void {
    this.router.navigate(['/delete', user.id]);
  }
}
