import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user.model'; 
import userData from '../../datas/userData.json'; 

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css'],
})
export class UserDeleteComponent {
  userId?: number;
  user?: User;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.userId = parseInt(this.route.snapshot.paramMap.get('id')!);

    this.user = userData.find((u) => u.id === this.userId);
  }

  deleteUser() {
    console.log('Deleted user:', this.user);

    this.router.navigate(['/users']);
  }

  goBack() {
    this.router.navigate(['/users']);
  }
}
