import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private apiUrl = 'https://65885deb90fa4d3dabf9d2a2.mockapi.io/Users';
  private usersSubject = new BehaviorSubject<User[]>([]);
  private users$ = this.usersSubject.asObservable();

  constructor(private http: HttpClient) {
    this.loadUsers();
  }

  private loadUsers(): void {
    this.http.get<User[]>(this.apiUrl).subscribe(
      (users) => {
        this.usersSubject.next(users);
      },
      (error) => {
        console.error('Error loading users:', error);
      }
    );
  }

  getUsers(): Observable<User[]> {
    return this.users$;
  }

  getUserById(userId: number): Observable<User | undefined> {
    return this.users$.pipe(
      map((users) => users.find((user) => user.id === userId))
    );
  }

  deleteUser(userId: number): void {
    const currentUsers = this.usersSubject.value;
    const updatedUsers = currentUsers.filter((user) => user.id !== userId);
    this.usersSubject.next(updatedUsers);
  }

  updateUser(updatedUser: User): void {
    const currentUsers = this.usersSubject.value;
    const updatedUsers = currentUsers.map((user) =>
      user.id === updatedUser.id ? updatedUser : user
    );
    this.usersSubject.next(updatedUsers);
  }
}
