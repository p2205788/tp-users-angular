import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user.model'; 
import userData from '../../datas/userData.json'; 

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css'],
})
export class UserUpdateComponent implements OnInit {
  userId?: number;
  user?: User;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.userId = parseInt(this.route.snapshot.paramMap.get('id')!);

    this.user = userData.find((u) => u.id === this.userId);
  }

  updateUserInfo(updatedUser: User) {
    this.router.navigate(['/user', this.userId]);
  }

  goBack() {
    this.router.navigate(['/user', this.userId]);
  }
}
