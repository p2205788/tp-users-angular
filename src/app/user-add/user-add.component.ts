import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css'],
})
export class AddUserComponent {
  newUser: any = {};

  constructor(private router: Router) {}

  addUser() {

    this.router.navigate(['/user-list']);
  }

  goBack() {
    this.router.navigate(['/user-list']);
  }
}
